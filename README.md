# DSA Challenge Task

This semester's challenge task (CT) is the design and implementation of a fully-distributed, P2P messaging application with an integrated, blockchain-based notary service with TomP2P(DHT), Web3j(Interface Java/ETH and Solidity (Smart Contracts).

## Maven

```bash
#run any checks to verify the package is valid and meets quality criteria
mvn verify
```