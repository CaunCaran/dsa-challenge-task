package local;

import static org.junit.jupiter.api.Assertions.assertTrue;

import hsr.dsa.local.DeserializerJSON;
import hsr.dsa.local.Profile;
import hsr.dsa.local.SerializerJSON;
import java.io.IOException;
import org.junit.jupiter.api.Test;

import java.io.File;

class DeserializerJSONTest {

    private Profile testProfileSerialize;
    private Profile testProfileDeserialize;
    private String pathFile = "./test.json";

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        testProfileSerialize = new Profile("", "");
    }

    @Test
    void writeAndReadJsonFile() throws IOException {
        String username = "myUsername";
        testProfileSerialize.setUsername(username);

        SerializerJSON serialJSON = new SerializerJSON(pathFile, testProfileSerialize);
        serialJSON.run();

        @SuppressWarnings("rawtypes")
		DeserializerJSON deserializeJSON = new DeserializerJSON(pathFile, testProfileSerialize.getClass());
        testProfileDeserialize = (Profile) deserializeJSON.run();

        assertTrue(testProfileDeserialize.getUsername().equals(username));
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        File jsonGenerated = new File(pathFile);
        jsonGenerated.delete();
    }
}
