package local;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import hsr.dsa.local.Profile;
import hsr.dsa.local.SerializerJSON;

class SerializerJSONTest {

	private Profile testProfile;
	private String pathFile = "./test.json";

	@org.junit.jupiter.api.BeforeEach
	void setUp() {
		testProfile = new Profile("", "");
	}

	@Test
	void generateJsonFile() {
		testProfile.setUsername("myUsername");

		SerializerJSON serialJSON = new SerializerJSON(pathFile, testProfile);
		try {
			serialJSON.run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@org.junit.jupiter.api.AfterEach
	void tearDown() {
		File jsonGenerated = new File(pathFile);
		jsonGenerated.delete();
	}
}