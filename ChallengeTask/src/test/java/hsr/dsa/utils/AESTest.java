package hsr.dsa.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AESTest {
	AES aes = null;

	@BeforeEach
	void setUp() throws Exception {
		aes = new AES();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void AESEncryptDecryptTest() throws Exception {
		String message = "Wurst Hans mit AES.";
		String key = "gummibaerli";
		assertEquals(aes.decrypt(aes.encrypt(message, key), key), "Wurst Hans mit AES.");
	}

}
