package hsr.dsa.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RSATest {

	RSA rsa;
	private String pathTestDirectory = "./test/";

	@BeforeEach
	void setUp() throws Exception {
		File profiles = new File(pathTestDirectory);
		if (!profiles.isDirectory()) {
			profiles.mkdirs();
		}

		this.rsa = new RSA(pathTestDirectory, "public.pub", "private.key");
	}

	@Test
	void RSAEncryptDecryptTest() throws Exception {
		String message = "Hans Wurst mit RSA.";
		assertEquals(this.rsa.decrypt(this.rsa.encrypt(message)), message);
	}

	@AfterEach
	void tearDown() throws Exception {
		FileUtils.deleteDirectory(new File(pathTestDirectory));
	}

}
