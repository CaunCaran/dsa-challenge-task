package hsr.dsa.blockchain.exceptions;

public class BlockChainException extends Exception {
	private static final long serialVersionUID = 5124736974689496506L;
	
	public BlockChainException(String message, Exception cause) {
		super(message, cause);
	}
}
