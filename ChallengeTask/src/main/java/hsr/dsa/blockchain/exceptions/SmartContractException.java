package hsr.dsa.blockchain.exceptions;

public class SmartContractException extends Exception {
	private static final long serialVersionUID = 2135556461705192419L;

	public SmartContractException(String message, Exception cause) {
		super(message, cause);
	}
}
