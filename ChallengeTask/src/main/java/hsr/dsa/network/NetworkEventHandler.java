package hsr.dsa.network;

import java.util.List;

import hsr.dsa.network.data.ChatMessage;
import hsr.dsa.network.data.DHTGroup;
import hsr.dsa.network.data.FriendRequest;
import hsr.dsa.network.data.GroupInvite;
import net.tomp2p.peers.Number160;

public interface NetworkEventHandler {
	public void onFriendRequest(FriendRequest friendRequest);
	public void onGroupInvite(GroupInvite groupInvite);
	public void onDirectMessage(ChatMessage message);
	public void onGroupMessage(ChatMessage message, DHTGroup group);
	public Number160 getListenAddress();
	public List<DHTGroup> getRegisteredGroups();
	public List<AcitveProfile> getFriendList();
}
