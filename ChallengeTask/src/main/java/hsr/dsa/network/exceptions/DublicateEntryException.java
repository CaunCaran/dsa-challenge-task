package hsr.dsa.network.exceptions;

public class DublicateEntryException extends Exception {
	private static final long serialVersionUID = -553137471759692318L;
	
	public DublicateEntryException(String message) {
		super(message);
	}
}
