package hsr.dsa.network;

import java.io.IOException;
import java.net.InetAddress;

import hsr.dsa.local.PropertiesManager;

public abstract class NetworkManager {
	private static DataClient dataClient;
	
	public static DataClient getDataClient() {
		if(dataClient == null) {
			PropertiesManager properties = PropertiesManager.getPropertiesManager();
			try {
				dataClient = new DataClient(Short.parseShort(properties.getProperty("client.port")), InetAddress.getByName(properties.getProperty("root.ip")), Short.parseShort(properties.getProperty("root.port")));
			} catch (NumberFormatException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return dataClient;
	}
}