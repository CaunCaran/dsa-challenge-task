package hsr.dsa.network;

import java.time.LocalDateTime;

import net.tomp2p.peers.Number160;

public interface AcitveProfile {
	public Number160 getId();
	public void setLastActivity(LocalDateTime lastActivity);
}
