package hsr.dsa.network.data;

import java.security.PublicKey;
import java.time.LocalDateTime;

import net.tomp2p.peers.Number160;

public class PublicProfile extends DataObject {
	private static final long serialVersionUID = 1568316768164996712L;

	private final String username;
	private PublicKey publicKey;
	private String blockChainPublicKey;
	private LocalDateTime lastActivity;
	
	public PublicProfile(String username, PublicKey publicKey, String blockChainPublicKey) {
		super(Number160.createHash(username));
		this.username = username;
		this.publicKey = publicKey;
		this.blockChainPublicKey = blockChainPublicKey;
		this.setLastActivity(LocalDateTime.now());
	}

	public String getUsername() {
		return username;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public String getBlockChainPublicKey() {
		return blockChainPublicKey;
	}

	public void setBlockChainPublicKey(String blockChainPublicKey) {
		this.blockChainPublicKey = blockChainPublicKey;
	}

	@Override
	public String toString() {
		return "PublicProfile [username=" + username + ", publicKey=" + publicKey.toString()
				+ ", blockChainPublicKex=" + blockChainPublicKey + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + blockChainPublicKey.hashCode();
		result = prime * result + publicKey.hashCode();
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PublicProfile other = (PublicProfile) obj;
		if (!blockChainPublicKey.equals(other.blockChainPublicKey))
			return false;
		if (publicKey.equals(other.publicKey))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public LocalDateTime getLastActivity() {
		return lastActivity;
	}
	
	public void updateLastActivity() {
		this.lastActivity = LocalDateTime.now();
	}

	public void setLastActivity(LocalDateTime lastActivity) {
		this.lastActivity = lastActivity;
	}
}
