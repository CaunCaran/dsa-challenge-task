package hsr.dsa.network.data;

import net.tomp2p.peers.Number160;

public class ChatMessage extends DataObject {
	private static final long serialVersionUID = 3926981622176992874L;
	
	private final long creationTime;
	private final String message;
	
	public ChatMessage(long creationTime, Number160 creator, String message) {
		super(creator);
		this.creationTime = creationTime;
		this.message = message;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public Number160 getCreator() {
		return this.getSender();
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "ChatMessage [creationTime=" + creationTime + ", creator=" + this.getSender() + ", message=" + message + "]";
	}
}
