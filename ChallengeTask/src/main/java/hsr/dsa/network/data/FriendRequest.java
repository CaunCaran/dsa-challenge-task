package hsr.dsa.network.data;

import net.tomp2p.peers.Number160;

public class FriendRequest extends DataObject {
	private static final long serialVersionUID = 1939793927811734593L;
	
	public enum RequestState {open, accepted, rejected}

	private final Number160 replyer;
	private final RequestState state;
	
	public FriendRequest(Number160 requester, Number160 replyer, RequestState state) {
		super(requester);
		this.replyer = replyer;
		this.state = state;
	}

	public Number160 getRequester() {
		return this.getSender();
	}

	public Number160 getReplyer() {
		return replyer;
	}

	public RequestState getState() {
		return state;
	}

	@Override
	public String toString() {
		return "FriendRequest [requester=" + this.getSender() + ", replyer=" + replyer + ", state=" + state + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((replyer == null) ? 0 : replyer.hashCode());
		result = prime * result + ((this.getSender() == null) ? 0 : this.getSender().hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FriendRequest other = (FriendRequest) obj;
		if (replyer == null) {
			if (other.replyer != null)
				return false;
		} else if (!replyer.equals(other.replyer))
			return false;
		if (this.getSender() == null) {
			if (other.getSender() != null)
				return false;
		} else if (!this.getSender().equals(other.getSender()))
			return false;
		if (state != other.state)
			return false;
		return true;
	}
}
