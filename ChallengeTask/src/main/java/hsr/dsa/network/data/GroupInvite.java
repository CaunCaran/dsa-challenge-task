package hsr.dsa.network.data;

import net.tomp2p.peers.Number160;

public class GroupInvite extends DataObject {
	private static final long serialVersionUID = -4139749393978039851L;

	private final Number160 group;
	private final String password;
	
	public GroupInvite(Number160 creator, Number160 group, String password) {
		super(creator);
		this.group = group;
		this.password = password;
	}

	public Number160 getCreator() {
		return this.getSender();
	}

	public Number160 getGroup() {
		return group;
	}

	public String getPassword() {
		return password;
	}
}
