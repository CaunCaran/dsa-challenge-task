package hsr.dsa.network.data;

import java.io.Serializable;

import net.tomp2p.peers.Number160;

public abstract class DataObject implements Serializable {
	private static final long serialVersionUID = 4964990691278631632L;
	private final Number160 sender;
	
	public DataObject(Number160 sender) {
		super();
		this.sender = sender;
	}
	
	public Number160 getSender() {
		return sender;
	}
}
