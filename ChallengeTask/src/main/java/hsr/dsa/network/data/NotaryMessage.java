package hsr.dsa.network.data;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.tomp2p.peers.Number160;

public class NotaryMessage extends ChatMessage {
	private static final long serialVersionUID = -8990507413385811203L;
	private final byte[] hash;

	public NotaryMessage(long creationTime, Number160 creator, String message) throws NoSuchAlgorithmException {
		super(creationTime, creator, message);
		this.hash = MessageDigest.getInstance("SHA-256").digest(message.trim().getBytes(StandardCharsets.UTF_8));
	}

	public byte[] getHash() {
		return hash;
	}
	
	@Override
	public String toString() {
		return "NotaryMessage [creationTime=" + this.getCreationTime() + ", creator=" + this.getSender() + ", message=" + this.getMessage() + ", hash=" + hash + "]";
	}
}
