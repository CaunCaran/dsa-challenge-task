package hsr.dsa.network.data;

import java.io.IOException;
import java.security.InvalidKeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import hsr.dsa.utils.ObjectUtils;
import hsr.dsa.utils.RSA;
import net.tomp2p.peers.Number160;

public class AsymmetricEncriptedMessage<T extends DataObject> extends DataObject {
	private static final long serialVersionUID = -3355987802546756937L;
	private final byte[] data;
	
	public AsymmetricEncriptedMessage(Number160 sender, T data, RSA ras) throws IOException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		super(sender);
		this.data = ras.encrypt(ObjectUtils.serialize(data));
	}
	
	@SuppressWarnings("unchecked")
	public T getValue(RSA ras) throws ClassNotFoundException, IOException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		return (T)ObjectUtils.deserialize(ras.decrypt(this.data));	
	}
}