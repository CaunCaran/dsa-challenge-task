package hsr.dsa.network.data;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import net.tomp2p.peers.Number160;

public class DHTGroup extends DataObject {
	private static final long serialVersionUID = -985064396677670046L;
	private final long id;
	private final String name;
	private final Set<Number160> members;
	
	public DHTGroup(Number160 sender, String name) {
		super(sender);
		Random random = new Random();
		this.id = random.nextLong();
		this.name = name;
		this.members = new HashSet<Number160>();
	}
	
	public DHTGroup(Number160 sender, long id, String name, Set<Number160> members) {
		super(sender);
		this.id = id;
		this.name = name;
		this.members = members;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<Number160> getMembers() {
		return members;
	}

	@Override
	public String toString() {
		return "DHTGroup [id=" + id + ", members=" + members + "]";
	}
}
