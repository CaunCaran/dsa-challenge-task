package hsr.dsa.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ObservableList<T> extends Observable implements List<T> {
	private final List<T> list;

	public ObservableList() {
		this.list = new ArrayList<T>();
	}
	
	public ObservableList(List<T> innerList) {
		this.list = innerList;
	}

	@Override
	public synchronized boolean add(T e) {
		boolean hasChanged = this.list.add(e);
		this.setChanged();
		return hasChanged;
	}

	@Override
	public synchronized void add(int index, T element) {
		this.list.add(index, element);
		this.setChanged();
	}

	@Override
	public synchronized boolean addAll(Collection<? extends T> c) {
		boolean hasChanged = this.list.addAll(c);
		if(hasChanged) {
			this.setChanged();
		}
		return hasChanged;
	}

	@Override
	public synchronized boolean addAll(int index, Collection<? extends T> c) {
		boolean hasChanged = this.list.addAll(index, c);
		if(hasChanged) {
			this.setChanged();
		}
		return hasChanged;
	}

	@Override
	public synchronized void clear() {
		this.list.clear();
		this.setChanged();
	}

	@Override
	public synchronized boolean contains(Object o) {
		return this.list.contains(o);
	}

	@Override
	public synchronized boolean containsAll(Collection<?> c) {
		return this.list.containsAll(c);
	}

	@Override
	public synchronized T get(int index) {
		return this.list.get(index);
	}

	@Override
	public synchronized int indexOf(Object o) {
		return this.list.indexOf(o);
	}

	@Override
	public synchronized boolean isEmpty() {
		return this.list.isEmpty();
	}

	@Override
	public synchronized Iterator<T> iterator() {
		return this.list.iterator();
	}

	@Override
	public synchronized int lastIndexOf(Object o) {
		return this.list.lastIndexOf(o);
	}

	@Override
	public synchronized ListIterator<T> listIterator() {
		return this.list.listIterator();
	}

	@Override
	public synchronized ListIterator<T> listIterator(int index) {
		return this.list.listIterator(index);
	}

	@Override
	public synchronized boolean remove(Object o) {
		boolean hasChanged = this.list.remove(o);
		if(hasChanged) {
			this.setChanged();
		}
		return hasChanged;
	}

	@Override
	public synchronized T remove(int index) {
		T element = this.list.remove(index);
		this.setChanged();
		return element;
	}

	@Override
	public synchronized boolean removeAll(Collection<?> c) {
		boolean hasChanged = this.list.removeAll(c);
		if(hasChanged) {
			this.setChanged();
		}
		return hasChanged;
	}

	@Override
	public synchronized boolean retainAll(Collection<?> c) {
		boolean hasChanged = this.list.retainAll(c);
		if(hasChanged) {
			this.setChanged();
		}
		return hasChanged;
	}

	@Override
	public synchronized T set(int index, T element) {
		T oldElement = this.list.set(index, element);
		this.setChanged();
		return oldElement;
	}

	@Override
	public synchronized int size() {
		return this.list.size();
	}

	@Override
	public synchronized List<T> subList(int fromIndex, int toIndex) {
		return this.list.subList(fromIndex, toIndex);
	}

	@Override
	public synchronized Object[] toArray() {
		return this.list.toArray();
	}

	@SuppressWarnings("hiding")
	@Override
	public synchronized <T> T[] toArray(T[] a) {
		return this.list.toArray(a);
	}
	
}
