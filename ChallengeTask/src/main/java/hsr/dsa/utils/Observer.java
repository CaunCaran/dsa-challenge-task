package hsr.dsa.utils;

public interface Observer {
	public void onChange(Observable observable);
}
