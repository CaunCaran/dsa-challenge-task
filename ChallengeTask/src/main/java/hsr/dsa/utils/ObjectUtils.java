package hsr.dsa.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import hsr.dsa.network.data.DataObject;

public abstract class ObjectUtils {
	public static byte[] serialize(DataObject object) throws IOException {
		byte[] data = null;
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			try (ObjectOutputStream out = new ObjectOutputStream(bos)) {
				out.writeObject(object);
				out.flush();
				data = bos.toByteArray();
			}
		}
		return data;
	}

	public static DataObject deserialize(byte[] data) throws IOException, ClassNotFoundException {
		DataObject object = null;
		try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(data))) {
			Object o = in.readObject();
			if (o instanceof DataObject) {
				object = (DataObject) o;
			}
		}
		return object;
	}
}
