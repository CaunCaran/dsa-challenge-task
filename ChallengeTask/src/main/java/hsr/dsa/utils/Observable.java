package hsr.dsa.utils;

import java.util.ArrayList;
import java.util.List;

public class Observable {
	private final List<Observer> observers;
	
	public Observable() {
		this.observers = new ArrayList<Observer>();
	}

	protected synchronized void setChanged() {
		Observable self = this;
		(new Thread() {
			@Override
			public void run() {
				for(Observer observer : observers) {
					observer.onChange(self);
				}
			}
		}).start();
	}
	
	public synchronized void addObserver(Observer observer) {
		this.observers.add(observer);
	}
	
	public synchronized void removeObserver(Observer observer) {
		this.observers.remove(observer);
	}
}
