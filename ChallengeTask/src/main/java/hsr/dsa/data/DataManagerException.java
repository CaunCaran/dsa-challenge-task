package hsr.dsa.data;

public class DataManagerException extends Exception {
	private static final long serialVersionUID = 2893718274499650879L;

	public DataManagerException(String message, Exception cause) {
		super(message, cause);
	}
}
