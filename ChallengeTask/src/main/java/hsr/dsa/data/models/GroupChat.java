package hsr.dsa.data.models;

import java.util.List;

public class GroupChat extends Chat {
	private final Group group;

	public GroupChat(List<ChatMessage> messages, Group group) {
		super(messages);
		this.group = group;
	}

	public Group getGroup() {
		return group;
	}
}
