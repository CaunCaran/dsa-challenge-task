package hsr.dsa.data.models;

import java.time.LocalDateTime;

import hsr.dsa.network.AcitveProfile;
import hsr.dsa.utils.Observable;
import hsr.dsa.utils.RSA;
import net.tomp2p.peers.Number160;

public abstract class User extends Observable implements AcitveProfile {
	private final Number160 id;
	private final String name;
	private final RSA rsa;
	private final String blockChainPublicKey;
	private LocalDateTime lastActivity;

	public User(Number160 id, String name, RSA rsa, String blockChainPublicKey) {
		super();
		this.id = id;
		this.name = name;
		this.rsa = rsa;
		this.blockChainPublicKey = blockChainPublicKey;
		this.lastActivity = LocalDateTime.MIN;
	}

	public String getName() {
		return name;
	}
	
	public Number160 getId() {
		return this.id;
	}
	
	public RSA getRas() {
		return this.rsa;
	}

	public String getBlockChainPublicKey() {
		return blockChainPublicKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public LocalDateTime getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(LocalDateTime lastActivity) {
		LocalDateTime prevActivity = this.lastActivity;
		this.lastActivity = lastActivity;
		if(!prevActivity.equals(lastActivity)) {
			this.setChanged();
		}
	}
}
