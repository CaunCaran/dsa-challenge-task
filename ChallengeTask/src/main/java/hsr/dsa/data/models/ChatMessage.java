package hsr.dsa.data.models;

import java.time.LocalDateTime;

import hsr.dsa.utils.Observable;

public abstract class ChatMessage extends Observable {
	private final User sender;
	private final LocalDateTime date;
	
	public ChatMessage(User sender, LocalDateTime date) {
		super();
		this.sender = sender;
		this.date = date;
	}

	public User getSender() {
		return sender;
	}
	
	public LocalDateTime getDate() {
		return date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((sender == null) ? 0 : sender.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChatMessage other = (ChatMessage) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (sender == null) {
			if (other.sender != null)
				return false;
		} else if (!sender.getId().equals(other.sender.getId()))
			return false;
		return true;
	}
	
	
}
