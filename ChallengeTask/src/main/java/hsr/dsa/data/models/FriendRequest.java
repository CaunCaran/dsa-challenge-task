package hsr.dsa.data.models;

public class FriendRequest {
	private final User requester;
	private final hsr.dsa.network.data.FriendRequest request;

	public FriendRequest(User requester, hsr.dsa.network.data.FriendRequest request) {
		super();
		this.requester = requester;
		this.request = request;
	}

	public User getRequester() {
		return this.requester;
	}

	public hsr.dsa.network.data.FriendRequest getRequest() {
		return request;
	}
}
