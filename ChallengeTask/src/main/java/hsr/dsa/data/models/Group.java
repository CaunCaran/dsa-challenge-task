package hsr.dsa.data.models;

import hsr.dsa.utils.ObservableList;

public class Group {
	private final long id; 
	private final String name;
	private final ObservableList<User> members;
	private final ObservableList<ChatMessage> chatMessages;

	public Group(long id, String name, ObservableList<User> members) {
		super();
		this.id = id;
		this.name = name;
		this.members = members;
		this.chatMessages = new ObservableList<ChatMessage>();
	}

	public long getId() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public ObservableList<User> getMembers() {
		return members;
	}

	public ObservableList<ChatMessage> getChatMessages() {
		return chatMessages;
	}
}
