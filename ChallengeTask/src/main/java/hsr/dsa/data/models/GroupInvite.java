package hsr.dsa.data.models;

public class GroupInvite {
	private final User requester;
	private final Group group;

	public GroupInvite(User requester, Group group) {
		super();
		this.requester = requester;
		this.group = group;
	}

	public User getRequester() {
		return requester;
	}

	public Group getGroup() {
		return group;
	}
}
