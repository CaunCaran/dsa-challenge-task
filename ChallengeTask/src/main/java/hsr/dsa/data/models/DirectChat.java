package hsr.dsa.data.models;

import java.util.List;

public class DirectChat extends Chat {
	private final User recipient;
	
	public DirectChat(List<ChatMessage> messages, User recipient) {
		super(messages);
		this.recipient = recipient;
	}

	public User getRecipient() {
		return recipient;
	}
}
