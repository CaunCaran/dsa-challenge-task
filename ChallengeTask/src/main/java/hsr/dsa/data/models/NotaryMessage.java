package hsr.dsa.data.models;

import java.time.LocalDateTime;

public class NotaryMessage extends ChatMessage {
	public enum State {uploaded, received, accepted, rejected}
	
	private final String message;
	private final byte[] hash;
	
	private LocalDateTime received;
	private LocalDateTime accepted;
	private State state;

	public NotaryMessage(User sender, LocalDateTime uploaded, String message, byte[] hash) {
		this(sender, uploaded, message, hash, null, null);
	}
	
	public NotaryMessage(User sender, LocalDateTime uploaded, String message, byte[] hash, LocalDateTime received, LocalDateTime accepted) {
		super(sender, uploaded);
		this.message = message;
		this.hash = hash;
		this.received = received;
		this.accepted = accepted;
		if(accepted != null) {
			this.state = State.accepted;
		} else if(received != null) {
			this.state = State.received;
		} else {
			this.state = State.uploaded;
		}
	}

	public String getMessage() {
		return message;
	}

	public LocalDateTime getReceived() {
		return received;
	}

	public void setReceived(LocalDateTime received) {
		if(this.state == State.uploaded && received != null) {
			this.received = received;
			this.state = State.received;
			this.setChanged();
		}
	}

	public LocalDateTime getAccepted() {
		return accepted;
	}

	public void setAccepted(LocalDateTime accepted) {
		if(this.state == State.received && accepted != null) {
			this.accepted = accepted;
			this.state = State.accepted;
			this.setChanged();
		}
	}

	public void setRejected(LocalDateTime accepted) {
		if(this.state == State.received && accepted != null) {
			this.accepted = accepted;
			this.state = State.rejected;
			this.setChanged();
		}
	}

	public State getState() {
		return state;
	}

	public byte[] getHash() {
		return hash;
	}
}
