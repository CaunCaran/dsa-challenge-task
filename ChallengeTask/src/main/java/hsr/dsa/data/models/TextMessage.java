package hsr.dsa.data.models;

import java.time.LocalDateTime;

public class TextMessage extends ChatMessage {
	private final String message;

	public TextMessage(User sender, LocalDateTime date, String message) {
		super(sender, date);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
