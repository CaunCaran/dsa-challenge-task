package hsr.dsa.data.models;

import hsr.dsa.utils.ObservableList;
import hsr.dsa.utils.RSA;
import net.tomp2p.peers.Number160;

public class RemoteUser extends User {
	private final ObservableList<ChatMessage> chatMessages;
	
	public RemoteUser(Number160 id, String name, ObservableList<ChatMessage> chatMessages, RSA rsa, String blockChainPublicKey) {
		super(id, name, rsa, blockChainPublicKey);
		this.chatMessages = chatMessages;
	}

	public ObservableList<ChatMessage> getChatMessages() {
		return chatMessages;
	}
}
