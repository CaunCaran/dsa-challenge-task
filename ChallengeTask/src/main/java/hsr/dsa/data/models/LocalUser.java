package hsr.dsa.data.models;

import hsr.dsa.data.models.FriendRequest;
import hsr.dsa.data.models.GroupInvite;
import hsr.dsa.utils.Observable;
import hsr.dsa.utils.ObservableList;
import hsr.dsa.utils.Observer;
import hsr.dsa.utils.RSA;
import net.tomp2p.peers.Number160;

public class LocalUser extends User implements Observer {
	private final ObservableList<User> friends;
	private final ObservableList<FriendRequest> friendRequests;
	private final ObservableList<Group> groups;
	private final ObservableList<GroupInvite> groupInvite;
	private final String blockChainPrivateKey;
	
	private final String password;
	
	public LocalUser(Number160 id, String name, ObservableList<User> friends, ObservableList<Group> groups, RSA rsa, String password, String blockChainPrivateKey, String blockChainPublicKey) {
		super(id, name, rsa, blockChainPublicKey);
		this.friends = friends;
		this.friendRequests = new ObservableList<FriendRequest>();
		this.groups = groups;
		this.groupInvite = new ObservableList<GroupInvite>();

		this.friends.addObserver(this);
		this.friendRequests.addObserver(this);
		this.groups.addObserver(this);
		this.groupInvite.addObserver(this);
		this.password = password;
		this.blockChainPrivateKey = blockChainPrivateKey;
	}

	public ObservableList<User> getFriends() {
		return friends;
	}

	public ObservableList<FriendRequest> getFriendRequests() {
		return friendRequests;
	}

	public ObservableList<Group> getGroups() {
		return groups;
	}

	public ObservableList<GroupInvite> getGroupInvite() {
		return groupInvite;
	}

	@Override
	public String toString() {
		return "LocalUser [friends=" + friends + ", getName()=" + getName() + "]";
	}

	@Override
	public void onChange(Observable observable) {
		this.setChanged();
	}

	public String getPassword() {
		return this.password;
	}

	public String getBlockChainPrivateKey() {
		return this.blockChainPrivateKey;
	}
}
