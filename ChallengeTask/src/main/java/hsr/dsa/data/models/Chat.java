package hsr.dsa.data.models;

import java.util.List;

public abstract class Chat {
	private final List<ChatMessage> messages;

	public Chat(List<ChatMessage> messages) {
		this.messages = messages;
	}

	public List<ChatMessage> getMessages() {
		return messages;
	}
}
