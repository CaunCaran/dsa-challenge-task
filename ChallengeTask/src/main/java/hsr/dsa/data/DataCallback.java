package hsr.dsa.data;

@FunctionalInterface
public interface DataCallback<T> {
	public void onData(T data);
}
