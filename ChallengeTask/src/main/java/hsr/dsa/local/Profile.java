package hsr.dsa.local;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

import hsr.dsa.utils.RSA;


public class Profile {

	private String username;
	private List<Friend> friends;
	private List<Group> groups;
	@JsonIgnore
	private transient RSA rsa;
	private final String blockChainPrivateKey;

	Profile() {
		friends = new ArrayList<>();
		groups = new ArrayList<>();
		this.blockChainPrivateKey = null;
	}

	public Profile(String username, String blockChainPrivateKey) {
		this.username = username;
		this.blockChainPrivateKey = blockChainPrivateKey;

		friends = new ArrayList<>();
		groups = new ArrayList<>();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Friend> getFriends() {
		return friends;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public void addGroup(Group newGroup) {
		groups.add(newGroup);
	}

	public void addFriend(Friend newFriend) {
		friends.add(newFriend);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Profile profile = (Profile) o;
		return Objects.equals(username, profile.username);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username);
	}

	public void setRsa(RSA rsa) {
		this.rsa = rsa;
	}

	public RSA getRsa() {
		return this.rsa;
	}

	public String getBlockChainPrivateKey() {
		return blockChainPrivateKey;
	}
}
