package hsr.dsa.local;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class DeserializerJSON<T> {

	private String pathName;
	@SuppressWarnings("rawtypes")
	private Class deserializedObject;

	@SuppressWarnings("rawtypes")
	public DeserializerJSON(String path, Class objectDeserial) {
		pathName = path;
		deserializedObject = objectDeserial;
	}

	@SuppressWarnings("unchecked")
	public Object run() {
		ObjectMapper mapper = new ObjectMapper().registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());

		try {

			// Convert JSON string from file to Object
			return mapper.readValue(new File(pathName), deserializedObject);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
