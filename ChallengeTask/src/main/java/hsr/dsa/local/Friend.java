package hsr.dsa.local;

import java.util.Objects;

public class Friend {

	private String friendName;
	private ChatHistory chatHistory;
	private final String blockChainAddress;

	public Friend() {
		this.blockChainAddress = null;
	}

	public Friend(String friendName, String blockChainAddress) {
		this.friendName = friendName;
		this.blockChainAddress = blockChainAddress;
	}

	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}

	public ChatHistory getChatHistory() {
		return chatHistory;
	}

	public void setChatHistory(ChatHistory chatHistory) {
		this.chatHistory = chatHistory;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Friend friend = (Friend) o;
		return Objects.equals(friendName, friend.friendName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(friendName);
	}

	public String getBlockChainAddress() {
		return blockChainAddress;
	}
}
