package hsr.dsa.local;

public class InvalidDataException extends Exception {
	private static final long serialVersionUID = 5760490825365575319L;

	public InvalidDataException(String username_is_not_defined) {
		super(username_is_not_defined);
	}
}
