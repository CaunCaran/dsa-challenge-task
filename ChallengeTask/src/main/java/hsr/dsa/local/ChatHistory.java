package hsr.dsa.local;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.TreeMap;

public class ChatHistory {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Europe/Berlin")
	private TreeMap<LocalDateTime, ChatMessage> messageHistory;

	@JsonIgnore
	public TreeMap<LocalDateTime, ChatMessage> getAllMessageHistory() {
		return messageHistory;
	}

	public void setMessageHistory(TreeMap<LocalDateTime, ChatMessage> messageHistory) {
		this.messageHistory = messageHistory;
	}
}
