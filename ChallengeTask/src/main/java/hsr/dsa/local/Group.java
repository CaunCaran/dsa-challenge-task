package hsr.dsa.local;

import java.util.Objects;

public class Group {

	private final long groupId;
	private final String name;
	private ChatHistory chatHistory;
	private String groupSecret;

	public Group(long id, String name) {
		this.groupId = id;
		this.name = name;
	}

	public Group() {
		this(0, null);
	}

	public long getGroupId() {
		return groupId;
	}

	public String getName() {
		return name;
	}

	public ChatHistory getChatHistory() {
		return chatHistory;
	}

	public void setChatHistory(ChatHistory chatHistory) {
		this.chatHistory = chatHistory;
	}

	public String getGroupSecret() {
		return groupSecret;
	}

	public void setGroupSecret(String groupSecret) {
		this.groupSecret = groupSecret;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Group group = (Group) o;
		return groupId == group.groupId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(groupId, groupSecret);
	}
}
