package hsr.dsa.local;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;


public class SerializerJSON {

  private String pathName;
  private Object serializeObject;

  /**
   *  Writes json file to storage
   *
   * @param path, the path where to write the file to
   *              Condition: File has to exist
   * @param serializeObj, object which should be written to storage
   *                      Condition: It has to be a valid object
   */
  public SerializerJSON(String path, Object serializeObj) {
    pathName = path;
    serializeObject = serializeObj;
  }

  /**
   * Saves a Json file
 * @throws IOException 
 * @throws JsonMappingException 
 * @throws JsonGenerationException 
   */
  public boolean run() throws JsonGenerationException, JsonMappingException, IOException  {
    ObjectMapper mapper = new ObjectMapper().registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());

    // Convert object to JSON string and save into a file directly
    mapper.writeValue(new File(pathName), serializeObject);
    return true;
  }
}

