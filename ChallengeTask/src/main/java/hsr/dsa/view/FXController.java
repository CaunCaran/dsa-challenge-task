package hsr.dsa.view;

import java.util.Map;

public interface FXController {
	public void setWindowManager(FXWindowManager windowManager);
	public void setParamater(Map<String, Object> paramater);
}
