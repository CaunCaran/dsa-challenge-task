package hsr.dsa.view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hsr.dsa.controller.ChatController;
import hsr.dsa.controller.ControllerManager;
import hsr.dsa.data.models.User;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

@SuppressWarnings("restriction")
public class CreateProfileController implements FXController {
	private static final Logger log = LoggerFactory.getLogger(CreateProfileController.class);
	private FXWindowManager windowManager;

	@FXML
	private TextField profilename;
	@FXML
	private PasswordField password;
	@FXML
	private PasswordField passwordRepeate;
	@FXML
	private PasswordField privateKey;

	@Override
	public void setWindowManager(FXWindowManager windowManager) {
		this.windowManager = windowManager;
	}

	public void createProfile() {
		if(password.getText().trim().length() > 0 && password.getText().equals(passwordRepeate.getText())) {
			try {
				ChatController controller = ControllerManager.getChatController();
				String profileName = profilename.getText();
				User profile = controller.createProfile(profileName, password.getText().trim(), privateKey.getText().trim());
				
				Map<String, Object> parameter = new HashMap<String, Object>();
				parameter.put("User", profile);
				this.windowManager.switchScene("/fxml/chatWindow/chatWindow.fxml", parameter);
			} catch (IOException e) {
				log.error("Error getting ProfileController", e);
			} catch (IllegalArgumentException e) {
				log.error("Invalid Username", e);
			} catch (Exception e) {
				log.error("Error creating User", e);
			}
		} else {
			log.error("Password is to short or doesnt match");
		}
	}

	public void cancle() {
		this.windowManager.switchScene("/fxml/start.fxml");
	}

	@Override
	public void setParamater(Map<String, Object> paramater) {

	}
}
