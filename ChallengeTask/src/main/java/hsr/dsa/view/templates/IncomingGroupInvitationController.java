package hsr.dsa.view.templates;

import java.io.IOException;
import java.util.List;

import hsr.dsa.controller.ChatController;
import hsr.dsa.controller.ControllerManager;
import hsr.dsa.data.models.GroupInvite;
import hsr.dsa.data.models.User;
import hsr.dsa.view.FXController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class IncomingGroupInvitationController implements FXTemplate<GroupInvite> {
	@FXML
	private Label username;
	@FXML
	private Label groupname;
	private GroupInvite groupInvite;
	
	@Override
	public void setControler(FXController controller) {}

	@FXML
	public void initialize() {
		if(this.groupInvite != null) {
			this.username.setText(this.groupInvite.getRequester().getName());
		}
	}
	
	@Override
	public void setData(GroupInvite groupInvite) {
		this.groupInvite = groupInvite;
		if(this.username != null) {
			this.username.setText(this.groupInvite.getRequester().getName());
		}
		if(this.groupname != null) {
			this.groupname.setText(this.groupInvite.getGroup().getName());
		}
	}
	
	public void accept() {
		try {
			ChatController chatController = ControllerManager.getChatController();
			List<User> friends = chatController.getProfile().getFriends();
			if(!friends.contains(this.groupInvite.getRequester())) {
				friends.add(this.groupInvite.getRequester());
			}
			chatController.getClient().acceptGroupInvitation(groupInvite);
			chatController.getProfile().getGroupInvite().remove(groupInvite);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reject() {
		try {
			ChatController chatController = ControllerManager.getChatController();
			chatController.getProfile().getGroupInvite().remove(groupInvite);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
