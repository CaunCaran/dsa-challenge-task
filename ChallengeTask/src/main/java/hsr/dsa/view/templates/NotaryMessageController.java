package hsr.dsa.view.templates;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import hsr.dsa.blockchain.BlockChainClient;
import hsr.dsa.blockchain.exceptions.SmartContractException;
import hsr.dsa.controller.ControllerManager;
import hsr.dsa.data.models.NotaryMessage;
import hsr.dsa.data.models.NotaryMessage.State;
import hsr.dsa.utils.Observable;
import hsr.dsa.utils.Observer;
import hsr.dsa.view.FXController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

@SuppressWarnings("restriction")
public class NotaryMessageController implements FXTemplate<NotaryMessage>, Observer {
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("hh:mm dd.mm.yyyy", Locale.GERMAN);
	private NotaryMessage textMessage;

	@FXML
	private Label author;
	@FXML
	private Label message;
	@FXML
	private Label date;
	@FXML
	private ImageView statusIcon;

	@FXML
	private Button notaryAcceptButton;
	@FXML
	private Button notaryRejectButton;

	@FXML
	public void initialize() {
		if (textMessage != null) {
			this.updateView();
		}
	}

	@Override
	public void setData(NotaryMessage textMessage) {
		this.textMessage = textMessage;
		this.textMessage.addObserver(this);
		if (this.textMessage != null && this.author != null && this.message != null && this.statusIcon != null) {
			this.updateView();
		}
	}

	private void updateView() {
		this.author.setText(textMessage.getSender().getName());
		this.message.setText(textMessage.getMessage());
		this.date.setText(DATE_FORMAT.format(textMessage.getDate()));

		this.updateStatus();
	}

	private void updateStatus() {
		Image image = null;
		if (this.textMessage.getState() == State.rejected) {
			image = new Image(NotaryMessageController.class.getResourceAsStream("/images/icons/rejected.png"));
			if(this.notaryAcceptButton != null && this.notaryAcceptButton != null) {
				this.notaryAcceptButton.setDisable(true);
				this.notaryRejectButton.setDisable(true);
			}
		} else if (this.textMessage.getState() == State.accepted) {
			image = new Image(NotaryMessageController.class.getResourceAsStream("/images/icons/confirmed.png"));
			if(this.notaryAcceptButton != null && this.notaryAcceptButton != null) {
				this.notaryAcceptButton.setDisable(true);
				this.notaryRejectButton.setDisable(true);
			}
		} else if (this.textMessage.getState() == State.received) {
			image = new Image(NotaryMessageController.class.getResourceAsStream("/images/icons/recived.png"));
			if(this.notaryAcceptButton != null && this.notaryAcceptButton != null) {
				this.notaryAcceptButton.setDisable(false);
				this.notaryRejectButton.setDisable(false);
			}
		} else {
			image = new Image(NotaryMessageController.class.getResourceAsStream("/images/icons/sent.png"));
			if(this.notaryAcceptButton != null && this.notaryAcceptButton != null) {
				this.notaryAcceptButton.setDisable(true);
				this.notaryRejectButton.setDisable(true);
			}
		}
		statusIcon.setImage(image);
	}

	@Override
	public void setControler(FXController controller) {
	}

	@Override
	public void onChange(Observable observable) {
		this.updateStatus();
	}
	
	public void notaryAccept() {
		this.notaryAcceptButton.setDisable(true);
		this.notaryRejectButton.setDisable(true);
		(new Thread() {
			@Override
			public void run() {
				try {
					BlockChainClient bcc = ControllerManager.getChatController().getBlockChainClient();
					bcc.acknowledge(textMessage.getHash(), true);
				} catch (IOException | SmartContractException e) {
					e.printStackTrace();
				}
			}
		}).start();
		
	}
	
	public void notaryReject() {
		this.notaryAcceptButton.setDisable(true);
		this.notaryRejectButton.setDisable(true);
		(new Thread() {
			@Override
			public void run() {
				try {
					BlockChainClient bcc = ControllerManager.getChatController().getBlockChainClient();
					bcc.acknowledge(textMessage.getHash(), false);
				} catch (IOException | SmartContractException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
