package hsr.dsa.view.templates;

import hsr.dsa.data.models.Group;
import hsr.dsa.view.ChatWindowController;
import hsr.dsa.view.FXController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class GroupListItemController implements FXTemplate<Group> {
	private ChatWindowController chatWindowController;
	private Group group;

	@FXML
	private Label groupname;

	@FXML
	public void initialize() {
		if (group != null) {
			this.groupname.setText(group.getName());
		}
	}

	@Override
	public void setControler(FXController controller) {
		if(controller instanceof ChatWindowController) {
			this.chatWindowController = (ChatWindowController) controller;
		}
	}

	@Override
	public void setData(Group group) {
		this.group = group;
		if (this.groupname != null) {
			this.groupname.setText(group.getName());
		}
	}

	public void chat() {
		this.chatWindowController.chat(this.group);
	}
}
