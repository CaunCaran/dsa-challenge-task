package hsr.dsa.view.templates;

import hsr.dsa.view.FXController;
import hsr.dsa.view.StartController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class LoginProfileControler implements FXTemplate<String> {
	private StartController startController;
	private String profile;

	@FXML
	private Label username;

	@FXML
	public void initialize() {
		if(profile != null) {
			this.username.setText(profile);
		}
	}

	@Override
	public void setControler(FXController controller) {
		startController = (StartController) controller;
	}

	@Override
	public void setData(String profile) {
		this.profile = profile;
		if(this.username != null) {
			this.username.setText(profile);
		}
	}

	public void login() {
		this.startController.login(this.profile);
	}
}
