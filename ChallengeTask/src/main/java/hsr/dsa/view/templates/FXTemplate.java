package hsr.dsa.view.templates;

import hsr.dsa.view.FXController;

public interface FXTemplate<T> {
	public void setControler(FXController controller);
	public void setData(T data);
}
