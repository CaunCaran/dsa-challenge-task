package hsr.dsa.view.templates;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

import hsr.dsa.data.models.TextMessage;
import hsr.dsa.view.FXController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class MessageController implements FXTemplate<TextMessage> {
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("hh:mm dd.mm.yyyy", Locale.GERMAN);
	private TextMessage textMessage;

	@FXML
	private Label author;
	@FXML
	private Label message;
	@FXML
	private Label date;

	@FXML
	public void initialize() {
		if (textMessage != null) {
			this.author.setText(textMessage.getSender().getName());
			this.message.setText(textMessage.getMessage());
			this.date.setText(DATE_FORMAT.format(textMessage.getDate()));
		}
	}

	@Override
	public void setData(TextMessage textMessage) {
		this.textMessage = textMessage;
		if (this.textMessage != null && this.author != null && this.message != null) {
			if(textMessage.getSender() != null) {
				this.author.setText(textMessage.getSender().getName());
			}
			this.message.setText(textMessage.getMessage());
			this.date.setText(DATE_FORMAT.format(textMessage.getDate()));
		}
	}

	@Override
	public void setControler(FXController controller) {}
}
