package hsr.dsa.view.templates;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import hsr.dsa.data.models.RemoteUser;
import hsr.dsa.utils.Observable;
import hsr.dsa.utils.Observer;
import hsr.dsa.view.ChatWindowController;
import hsr.dsa.view.FXController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class FriendController implements FXTemplate<RemoteUser>, Observer {
	private static final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
	private ChatWindowController chatWindowController;
	private RemoteUser friend;

	@FXML
	private Label username;
	@FXML
	private Label lastActivity;

	@FXML
	public void initialize() {
		if (friend != null) {
			this.username.setText(friend.getName());
		}
	}

	@Override
	public void setControler(FXController controller) {
		chatWindowController = (ChatWindowController) controller;
	}

	@Override
	public void setData(RemoteUser friend) {
		this.friend = friend;
		this.friend.addObserver(this);
		if (this.username != null) {
			this.username.setText(friend.getName());
		}
		service.scheduleWithFixedDelay(() -> {
			Platform.runLater(() -> {
				this.updateActivity();
			});
		}, 0, 1, TimeUnit.SECONDS);
	}

	public void chat() {
		this.chatWindowController.chat(this.friend);
	}

	@Override
	public void onChange(Observable observable) {
		Platform.runLater(() -> {
			this.updateActivity();
		});
	}

	private void updateActivity() {
		if (this.lastActivity != null) {
			LocalDateTime tempDateTime = LocalDateTime.from(this.friend.getLastActivity());
			LocalDateTime now  = LocalDateTime.now();

			long years = tempDateTime.until(now, ChronoUnit.YEARS);
			tempDateTime = tempDateTime.plusYears(years);
			
			long months = tempDateTime.until(now, ChronoUnit.MONTHS);
			tempDateTime = tempDateTime.plusMonths(months);

			long days = tempDateTime.until(now, ChronoUnit.DAYS);
			tempDateTime = tempDateTime.plusDays(days);


			long hours = tempDateTime.until(now, ChronoUnit.HOURS);
			tempDateTime = tempDateTime.plusHours(hours);

			long minutes = tempDateTime.until(now, ChronoUnit.MINUTES);
			tempDateTime = tempDateTime.plusMinutes(minutes);
			
			long seconds = tempDateTime.until(now, ChronoUnit.SECONDS);
			
			if(years > 10) {
				this.lastActivity.setText("Offline");
			} else if(years > 0) {
				this.lastActivity.setText(String.format("Offline for %d years", years));
			} else if(months > 0) {
				this.lastActivity.setText(String.format("Offline for %d months", months));
			} else if(days > 0) {
				this.lastActivity.setText(String.format("Offline for %d days", days));
			} else if(hours > 0) {
				this.lastActivity.setText(String.format("Offline for %d hours", hours));
			} else if(minutes > 0) {
				this.lastActivity.setText(String.format("Offline for %d minutes", minutes));
			} else if(seconds > 10) {
				this.lastActivity.setText(String.format("Offline for %d seconds", seconds));
			} else {
				this.lastActivity.setText("Online");
			}
		}
	}
}
