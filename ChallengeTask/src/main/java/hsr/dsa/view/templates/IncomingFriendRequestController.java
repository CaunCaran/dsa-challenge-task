package hsr.dsa.view.templates;

import java.io.IOException;
import java.util.List;

import hsr.dsa.controller.ChatController;
import hsr.dsa.controller.ControllerManager;
import hsr.dsa.data.models.FriendRequest;
import hsr.dsa.data.models.User;
import hsr.dsa.view.FXController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class IncomingFriendRequestController implements FXTemplate<FriendRequest> {
	@FXML
	private Label username;
	private FriendRequest friendRequest;
	
	@Override
	public void setControler(FXController controller) {}

	@FXML
	public void initialize() {
		if(this.friendRequest != null) {
			this.username.setText(this.friendRequest.getRequester().getName());
		}
	}
	
	@Override
	public void setData(FriendRequest friendRequest) {
		this.friendRequest = friendRequest;
		if(this.username != null) {
			this.username.setText(this.friendRequest.getRequester().getName());
		}
	}
	
	public void accept() {
		try {
			ChatController chatController = ControllerManager.getChatController();
			List<User> friends = chatController.getProfile().getFriends();
			if(!friends.contains(this.friendRequest.getRequester())) {
				friends.add(this.friendRequest.getRequester());
			}
			chatController.getClient().acceptFriendRequest(friendRequest);
			chatController.getProfile().getFriendRequests().remove(friendRequest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reject() {
		try {
			ChatController chatController = ControllerManager.getChatController();
			chatController.getClient().rejectFriendRequest(friendRequest);
			chatController.getProfile().getFriendRequests().remove(friendRequest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
