package hsr.dsa.view.templates;

import hsr.dsa.data.models.User;
import hsr.dsa.view.FXController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@SuppressWarnings("restriction")
public class GroupMemberController implements FXTemplate<User> {
	private User friend;

	@FXML
	private Label username;

	@FXML
	public void initialize() {
		if (friend != null) {
			this.username.setText(friend.getName());
		}
	}

	@Override
	public void setControler(FXController controller) {}

	@Override
	public void setData(User profile) {
		this.friend = profile;
		if (this.username != null) {
			this.username.setText(profile.getName());
		}
	}
}
