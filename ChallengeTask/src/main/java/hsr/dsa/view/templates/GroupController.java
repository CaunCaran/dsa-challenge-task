package hsr.dsa.view.templates;

import java.io.IOException;

import hsr.dsa.controller.ChatController;
import hsr.dsa.controller.ControllerManager;
import hsr.dsa.data.models.Group;
import hsr.dsa.data.models.LocalUser;
import hsr.dsa.data.models.GroupInvite;
import hsr.dsa.utils.Observable;
import hsr.dsa.utils.ObservableList;
import hsr.dsa.utils.Observer;
import hsr.dsa.view.FXController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class GroupController implements FXTemplate<LocalUser>, Observer {
	private FXController fxController;
	private ChatController chatController;
	private ObservableList<Group> groups;
	private ObservableList<GroupInvite> groupsInvite;

	@FXML
	private VBox groupList;
	@FXML
	private TextField groupName;

	@FXML
	public void initialize() throws IOException {
		this.chatController = ControllerManager.getChatController();
	}

	@FXML
	public void create() throws ClassNotFoundException, IOException {
		(new Thread() {
			@Override
			public void run() {
				try {
					String name = groupName.getText();
					groupName.setDisable(true);
					chatController.createGroup(name);
					groupName.setText("");
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				} finally {
					Platform.runLater(() -> {
						groupName.setDisable(false);
					});
				}
			}
		}).start();
	}

	@Override
	public void onChange(Observable observable) {
		Platform.runLater(() -> {
			this.groupName.setDisable(false);
			this.groupList.getChildren().retainAll(this.groupList.getChildren().get(0));
			this.updateGroups();
			this.updateInvitationsGroups();
		});
	}

	@Override
	public void setControler(FXController fxController) {
		this.fxController = fxController;
	}

	@Override
	public void setData(LocalUser user) {
		this.groups = user.getGroups();
		this.groups.addObserver(this);
		this.groupsInvite = user.getGroupInvite();
		this.groupsInvite.addObserver(this);
		this.updateGroups();
	}

	@SuppressWarnings("unchecked")
	private void updateGroups() {
		try {
			for (Group group : this.groups) {
				FXMLLoader loader = new FXMLLoader();
				Parent groupListItem = (Parent) loader.load(getClass().getResourceAsStream("/fxml/chatWindow/templates/groupListItem.fxml"));
				FXTemplate<Group> controler = (FXTemplate<Group>) loader.getController();
				controler.setControler(fxController);
				controler.setData(group);
				this.groupList.getChildren().add(groupListItem);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void updateInvitationsGroups() {
		try {
			for (GroupInvite groupInvite : this.groupsInvite) {
				FXMLLoader loader = new FXMLLoader();
				Parent groupListItem = (Parent) loader.load(getClass().getResourceAsStream("/fxml/chatWindow/templates/incomingGroupInvitation.fxml"));
				FXTemplate<GroupInvite> controler = (FXTemplate<GroupInvite>) loader.getController();
				controler.setControler(fxController);
				controler.setData(groupInvite);
				this.groupList.getChildren().add(groupListItem);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
