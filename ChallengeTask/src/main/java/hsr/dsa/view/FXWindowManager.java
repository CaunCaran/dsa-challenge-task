package hsr.dsa.view;

import java.util.Map;

import javafx.stage.Stage;

@SuppressWarnings("restriction")
public interface FXWindowManager {
	public void switchScene(String sceneLocation);
	public void switchScene(String sceneLocation, Map<String, Object> paramater);
	public Stage getStage();
}
