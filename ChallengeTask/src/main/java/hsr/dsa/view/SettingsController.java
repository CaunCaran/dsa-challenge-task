package hsr.dsa.view;

import java.util.Map;

import hsr.dsa.local.PropertiesManager;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

@SuppressWarnings("restriction")
public class SettingsController implements FXController {
	private FXWindowManager windowManager;
	private PropertiesManager properties;

	@FXML
	private TextField rootIP;

	@FXML
	private TextField rootPort;

	@FXML
	private TextField clientPort;

	@Override
	public void setWindowManager(FXWindowManager windowManager) {
		this.windowManager = windowManager;
	}
	
	@FXML
    public void initialize() {
        this.properties = PropertiesManager.getPropertiesManager();
        this.rootIP.setText(this.properties.getProperty("root.ip"));
        this.rootPort.setText(this.properties.getProperty("root.port"));
        this.clientPort.setText(this.properties.getProperty("client.port"));
    }

	public void applySettings() {
		this.properties.setProperty("root.ip", this.rootIP.getText());
		this.properties.setProperty("root.port", this.rootPort.getText());
		this.properties.setProperty("client.port", this.clientPort.getText());
		this.windowManager.switchScene("/fxml/start.fxml");
	}

	public void cancleSettings() {
		this.windowManager.switchScene("/fxml/start.fxml");
	}

	@Override
	public void setParamater(Map<String, Object> paramater) {}
}
