package hsr.dsa.view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import hsr.dsa.controller.ChatController;
import hsr.dsa.controller.ControllerManager;
import hsr.dsa.view.templates.FXTemplate;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class StartController implements FXController {
	private FXWindowManager windowManager;
	private final ChatController profileController;

	public StartController() throws IOException {
		this.profileController = ControllerManager.getChatController();
	}

	@FXML
	private VBox profileList;

	@Override
	public void setWindowManager(FXWindowManager windowManager) {
		this.windowManager = windowManager;
	}

	@FXML
	@SuppressWarnings("unchecked")
	public void initialize() throws IOException {
		for (String user : profileController.getLoginProfiles()) {
			FXMLLoader loader = new FXMLLoader();
			Parent loginProfile = (Parent) loader
					.load(getClass().getResourceAsStream("/fxml/templates/loginProfile.fxml"));
			FXTemplate<String> templateControler = (FXTemplate<String>) loader.getController();
			templateControler.setData(user);
			templateControler.setControler(this);
			this.profileList.getChildren().add(loginProfile);
		}
	}

	public void showCreateProfile() {
		this.windowManager.switchScene("/fxml/createProfile.fxml");
	}

	public void showSettings() {
		this.windowManager.switchScene("/fxml/settings.fxml");
	}

	public void login(String profile) {
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("User", profile);
		this.windowManager.switchScene("/fxml/loginPassword.fxml", parameter);
	}

	@Override
	public void setParamater(Map<String, Object> paramater) {

	}
}
