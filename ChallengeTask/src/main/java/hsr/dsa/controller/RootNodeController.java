package hsr.dsa.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hsr.dsa.local.PropertiesManager;
import hsr.dsa.network.Node;

public class RootNodeController {
	private static final Logger log = LoggerFactory.getLogger(RootNodeController.class);
	private final Node node;
	
	public RootNodeController() throws NumberFormatException, UnknownHostException, IOException {
		PropertiesManager properties = PropertiesManager.getPropertiesManager();
		this.node = new Node(Short.parseShort(properties.getProperty("root.port")), InetAddress.getByName(properties.getProperty("root.ip")));
	}

	public void stop() {
		try {
			this.node.close();
		} catch (IOException e) {
			log.error("Error shuting down", e);
		}
	}
}
