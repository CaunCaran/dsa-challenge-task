package hsr.dsa.controller;

import java.io.IOException;

public class ControllerManager {
	private static ChatController chatController;

	public static ChatController getChatController() throws IOException {
		if(chatController == null) {
			chatController = new ChatController();
		}
		return chatController;
	}

	public static void stop() {
		chatController.stop();
	}
}
